[MariaDB](https://mariadb.org/) server.

# Volumes
- `/var/lib/mysql/data`

# Environment Variables
## MARIADB_ROOT_PASSWORD

Password for the root user.

# Environment Variables
## MARIADB_ADMIN_PASSWORD

Password for the admin user (remote connect enabled).

# Ports
- 3306
