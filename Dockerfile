FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN apk add --no-cache mariadb mariadb-client

RUN mkdir /run/mysqld
RUN chown mysql:mysql /run/mysqld

ADD /rootfs /

VOLUME /var/lib/mysql/data

EXPOSE 3306
